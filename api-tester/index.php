<?php
	$domain_url = "localhost/markwoollen.com/api.php?action=updateProject";
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>API - TESTER</title>
<style>
	#viewport {
		display: flex;
		flex-direction: row;
		width:95%;
		margin:0 auto;
	}

	.col {
		width:25%;
		box-sizing: border-box;
	}

</style>

<script type="text/javascript">

	function sendRequest(dest, data, requestMethod) {


		var initObj = this;
		var dataRes = null;

		var dataPromise = this.xhrPromise(data.value, dest.value, requestMethod);

		dataPromise.then(function(result) {
			console.log(result);
			document.getElementById('jsonresult').innerHTML = result.target.response;

		}).catch(function(error) {
			console.log("Failed: ", error);
		});

	}

	function xhrPromise(data, dest, requestMethod) {

		var initObj = this;

		return new Promise(function(resolve, reject) {

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = true;
			xhr.open(requestMethod, dest, true);
			xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			xhr.onload = resolve;
			xhr.onerror = reject;
			xhr.send(data);
			//xhr.send('data='+encodeURIComponent(data));

		});

	}


</script>

</head>

<body>
	<div id="viewport">
		<div class="col">
	        <h2>$_POST</h2>
			URL: <input type="text" id="url1" value="http://<?php echo $domain_url; ?>" size="100" /><br />
	        JSON: <textarea id="json1"  rows="15" cols="40"></textarea><br />
	        <button id="send" onClick="sendRequest(document.getElementById('url1'),document.getElementById('json1'),'POST');">send json</button>
	        <br /><br />
	        <h2>$_PUT</h2>
	        URL: <input type="text" id="url2" value="http://<?php echo $domain_url; ?>" size="100" /><br />
	        JSON: <textarea id="json2"  rows="15" cols="40"></textarea><br />
	        <button id="send" onClick="sendRequest(document.getElementById('url2'),document.getElementById('json2'),'PUT');">send json</button>
	        <br /><br />
	        <h2>$_DELETE</h2>
	        URL: <input type="text" id="url3" value="http://<?php echo $domain_url; ?>" size="100" /><br />
	        JSON: <textarea id="json3"  rows="15" cols="40"></textarea><br />
	        <button id="send" onClick="sendRequest(document.getElementById('url3'),document.getElementById('json3'),'DELETE');">send json</button>
	        <br /><br />
	        <h2>$_GET</h2>
	        URL: <input type="text" id="url4" value="http://<?php echo $domain_url; ?>" size="100" /><br />
	        JSON: <textarea id="json4"  rows="15" cols="40"></textarea><br />
	        <button id="send" onClick="sendRequest(document.getElementById('url4'),document.getElementById('json4'),'GET');">send json</button>
        </div>
		<div class="col">
	        <h1>RESPONSE</h1>
	        <pre id="jsonresult"></pre>
		</div>
    </div>
</body>
</html>

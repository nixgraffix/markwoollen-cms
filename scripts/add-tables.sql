-- --------------------------------------------------------

--
-- Table structure for table `Assets`
--

CREATE TABLE `assets` (
  `assetID` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `fileName` varchar(256) NOT NULL,
  `label` varchar(128) DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedOn` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `aortOrder` int(3) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `Projects`
--

CREATE TABLE `projects` (
  `projectID` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `posterImg` varchar(128) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedOn` datetime NOT NULL,
  `sortOrder` int(5) NOT NULL,
  `page` varchar(24) NOT NULL DEFAULT 'work',
  `publish` tinyint(1) NOT NULL DEFAULT '1',
  `details` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `ProjectAssets`
--

CREATE TABLE `projectAssets` (
  `projectID` int(11) NOT NULL,
  `assetID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Indexes for table `Assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`assetID`);


--
-- Indexes for table `ProjectAssets`
--
ALTER TABLE `projectAssets`
  ADD PRIMARY KEY (`projectID`,`assetID`),
  ADD KEY `assetID` (`assetID`);

--
-- Indexes for table `Projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`projectID`);
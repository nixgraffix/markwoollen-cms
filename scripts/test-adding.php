<?php

    require_once '../config.php';
    require_once '../Database.php';

    session_start();


    $json = file_get_contents('php://input');
    $temp = json_decode($json, true);
    $error = array();
    $result = array();

    $action = "addProject";

    $data = array(
        'id' => isset($temp['projectID']) ? $temp['projectID'] : null,
        'title' => "a6",
        'page' => "work",
        'publish' => "0",
        'trailers' => null       
    );

    if($action == "addProject"){
        
        try {
            $project = Database::run(
                "INSERT INTO projects (title, page, publish)
                    VALUES (?, ?, ?)", array($data['title'], $data['page'], $data['publish']));

                //$data['id'] = Database::__callStatic("lastInsertId", array());
                //$result['projectTable'] =  "Added to projects table id: " . $data['id'];
        
        }catch (Exception $e) {
            $error['projectTable'] =  "Can't add projects table: " .$e->getMessage();
        }
    }



    $result = json_encode($result);
    $error = json_encode($error);

    echo $result;
    echo "<br>";
    echo $error;

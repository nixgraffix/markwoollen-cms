<?php

    include('../config.php');


    function pdo_connect() {
        try {
            $connection = new PDO('mysql:dbname='.DB_NAME.';host='.DB_HOST, DB_USER, DB_PASS);
        }
    
        catch (PDOException $e){
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
        }
    
        return $connection;
    }

    date_default_timezone_set('America/Los_Angeles');

    $conn = pdo_connect();

    $sql = 'SELECT 		uniqueID, name, id, date, sortOrder, placement, onSite
            FROM     	trailers';

    $statement = $conn->prepare($sql);
    $statement->execute();
    $trailers = $statement->fetchAll(PDO::FETCH_ASSOC);

    ?>
        <div id="assets">
            <ul>
    <?php


    $i=1;

    foreach($trailers as $t){
        if(!empty($t['uniqueID'])){

            $publish = $t['onSite'] === 'true'? 1: 0;

            echo 'INSERT INTO projects (projectID, title, posterImg, createdOn, sortOrder, page, publish)<br>';
            echo 'values('.$t['uniqueID'].', "' .mb_convert_encoding(addslashes($t['name']), "UTF-8"). '", "'.addslashes($t['id']).'.jpg", "'.date("Y-m-d H:i:s", strtotime($t['date'])).'", "'.$t['sortOrder'].'", "'.$t['placement'].'", '.$publish.');<br>';

            $conn = pdo_connect();

            $sql = 'SELECT 		uniqueID, pos1_id, pos1_label, pos2_id, pos2_label, pos3_id, pos3_label, pos4_id, pos4_label, pos5_id, pos5_label, pos6_id, pos6_label, pos7_id, pos7_label, pos8_id, pos8_label
                    FROM     	linked_trailers
                    WHERE       uniqueID = '.$t['uniqueID'];

            $statement = $conn->prepare($sql);

            $statement->execute();
            $assets = $statement->fetchAll(PDO::FETCH_ASSOC);
            $conn = null;

            if(empty($assets)){
                echo 'INSERT INTO assets (assetID, title, fileName, label, sortOrder)<br>';
                echo 'values('.$i.', "' .mb_convert_encoding(addslashes($t['name']), "UTF-8"). ' Main Trailer", "'.$t['id'].'", "Main Trailer", 0);<br>';

                echo 'INSERT INTO projectAssets (projectID, assetID)<br>';
                echo 'values('.$t['uniqueID'].', '.$i.');<br>';

                $i++;

            }else{

                foreach($assets as $a){
                    $sort=0;

                    for($h=1;$h<=8;$h++) {

                        if(!empty($a['pos'.$h.'_id'])){

                            echo 'INSERT INTO assets (assetID, title, fileName, label, sortOrder)<br>';
                            echo 'values('.$i.', "' .$t['name']. ' '.addslashes($a['pos'.$h.'_label']).'", "'.$a['pos'.$h.'_id'].'", "'.addslashes($a['pos'.$h.'_label']).'", '.$sort++.');<br>';

                            echo 'INSERT INTO projectAssets (projectID, assetID)<br>';
                            echo 'values('.$t['uniqueID'].', '.$i.');<br>';

                            $i++;
                        }
                    }
                }
            }
        }

        echo '<br>';
    }

?>
        </ul>
    </div>


import React, { Component } from 'react';
import Nav from '../components/nav/Nav';
import List from '../components/list/List';
import Workspace from '../components/workspace/Workspace';
import Modal from '../components/modal/Modal';
import 'whatwg-fetch';
import './Main.css';

const WORKSPACES = ["Projects", "Trailers"];
const PAGES = ["work", "more"];

class Main extends Component {

    state = {
        currentWorkspace: WORKSPACES[0],
        workspaceData: null,
        listItems : [],
        saveResult : null,
        error : null,
        workSpaceAction : null
    }

    componentDidMount(){
        this.changeWorkspace(this.state.currentWorkspace);
    }

    changeWorkspace = (workspace) => {
        
        this.setState({
            currentWorkspace: workspace,
            workspaceData: null,
            workSpaceAction: "update"
        });
    
        fetch(window.location.protocol + "//localhost/markwoollen.com/api.php?action=getList&query=" + workspace)
        .then(this.handleResponse)
        .then((result) => {
            this.setState({
                isLoaded: true,
                listItems: result 
            });
        })
        .catch(error => {
            this.setState({
                isLoaded: true,
                listItems: {},
                error: error
            });
        })
    }

    handleAddNewClick = () => {
        
        if(this.state.currentWorkspace == "Projects"){
            this.setState({
                workspaceData: {
                    projectTitle : "",
                    page: "work",
                    publish: 0,
                    trailers: []
                },
                workSpaceAction : "add"
            });
        }else if(this.state.currentWorkspace == "Trailers"){
            this.setState({
                workspaceData: {
                    posterFile : "",
                    id: null,
                    label: "",
                    fileName: "",
                    sortOrder: 0,
                    thumbnailBlob: null,
                    title: "",
                    updatedOn: null
                },
                workSpaceAction : "add"
            });
        }
    }

    handleSave = (event) => {

        let action;

        if(this.state.workSpaceAction == "update"){
            action = { 
                Projects : "updateProject",
                Trailers : "updateTrailer"
            }
        }else if(this.state.workSpaceAction == "add"){
            action = { 
                Projects : "addProject",
                Trailers : "addTrailer"
            }
        }
        
        event.preventDefault();
        
        fetch(window.location.protocol + "//localhost/markwoollen.com/api.php?action=" + action[this.state.currentWorkspace], {
            method: "POST",
            headers : {
                "Content-type": "application/x-www-form-urlencoded"
            },
            body: JSON.stringify(this.state.workspaceData)
        })
        .then(this.handleResponse)
        .then((result) => {
            this.setState({
                isLoaded: true,
                saveResult: result,
                workSpaceAction: "update"
            });
        })
        .then(this.changeWorkspace(this.state.currentWorkspace))
        .catch(error => {
            this.setState({
                isLoaded: true,
                saveResult: {},
                error: error
            });
        })
    }

    handleDelete = (event) => {

        let action = { 
            Projects : "deleteProject",
            Trailers : "deleteTrailer"
        }

        event.preventDefault();

        fetch(window.location.protocol + "//localhost/markwoollen.com/api.php?action=" + action[this.state.currentWorkspace], {
            method: "POST",
            headers : {
                "Content-type": "application/x-www-form-urlencoded"
            },
            body: JSON.stringify(this.state.workspaceData)
        })
        .then(this.handleResponse)
        .then((result) => {
            this.setState({
                isLoaded: true,
                saveResult: result,
                workSpaceAction: "update"
            });
        })
        .then(this.changeWorkspace(this.state.currentWorkspace))
        .catch(error => {
            this.setState({
                isLoaded: true,
                saveResult: {},
                error: error
            });
        })
    }

    uploadFile = (file) => {
        
        fetch(window.location.protocol + "//localhost/markwoollen.com/api.php?action=updateTrailerThumbnail", {
            method: "POST",
            body: file
        })
        .then(this.handleResponse)
        .then((result) => {
            this.setState({
                isLoaded: true,
                saveResult: result 
            });
        })
        .catch(error => {
            this.setState({
                isLoaded: true,
                saveResult: {},
                error: error
            });
        })
    }

    changeAsset = (asset) =>  {

        if(this.state.currentWorkspace === "Trailers"){
            fetch(window.location.protocol + "//localhost/markwoollen.com/api.php?action=getTrailer&query=" + asset.id)
            .then(this.handleResponse)
            .then((result) => {
                result[0].thumbnailBlob = null; // add this for uploading thumbnail files
                this.setState({
                    workspaceData: result[0],
                    workSpaceAction: "update"
                });
            })
            .catch(error => {
                this.setState({workspaceData: null, error: error});
            })

        }else if(this.state.currentWorkspace === "Projects"){
            fetch(window.location.protocol + "//localhost/markwoollen.com/api.php?action=getProject&query=" + asset.id)
            .then(this.handleResponse)
            .then((result) => {
                this.setState({
                    workspaceData: result,
                    workSpaceAction: "update"
                });
            })
            .catch(error => {
                this.setState({workspaceData: null, error: error});
            })
        }
    }

    handleResponse (response) {
        return response.json()
            .then(json => {
                if (response.ok) {
                    return json
                } else {
                    return Promise.reject(Object.assign({}, json, {
                    status: response.status,
                    statusText: response.statusText
                }))
            }
        })
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        let workspaceData = {...this.state.workspaceData};

        // if input is from trailerSelector which has attr: data-array-key
        if(target.dataset.arrayKey) {
            let temp = {assetID : Number(value)}
            workspaceData.trailers[target.dataset.arrayKey] = temp;    
            
            this.setState({
                workspaceData
            });
                   
        }else if(target.type === "file" && target.files.length) {
            let reader = new FileReader();
            reader.onloadend = () => {
                workspaceData[name] = reader.result;
                this.setState({
                    workspaceData
                });
            }
            reader.readAsDataURL(target.files[0]);
        }else{
            workspaceData[name] = value;
            this.setState({
                workspaceData
            });
        }
    }
    
    render(){
        return(
            <div className="Main">
                <Nav currentWorkspace={this.state.currentWorkspace} workspaces={WORKSPACES} changeWorkspace={this.changeWorkspace} handleAddNewClick={this.handleAddNewClick}/>
                <List currentWorkspace={this.state.currentWorkspace} workspaceData={this.state.workspaceData} listItems={this.state.listItems} changeAsset={this.changeAsset}/>
                <Workspace allTrailers={this.state.listItems} currentWorkspace={this.state.currentWorkspace} workspaceData={this.state.workspaceData} listItems={this.state.listItems} pages={PAGES} handleChange ={this.handleInputChange} handleSave={this.handleSave} handleDelete={this.handleDelete}/>
                <Modal data={this.state.saveResult}/>
            </div>
        );
    }
}

export default Main;

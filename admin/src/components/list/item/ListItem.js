import React from 'react';
import './ListItem.css';

const ListItem = (props) => {
    // TODO: this active class toggler not working
    let classString = props.data === props.workspaceData ? 'ListItem active' : 'ListItem' ;

    return(
        <li className={classString} onClick={() => props.handleOnClick(props.data)}>
            {props.data.title}
        </li>
    )
};
    
export default ListItem;
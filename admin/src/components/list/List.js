import React from 'react';
import ListItem from './item/ListItem';
import './List.css';

const List = (props) =>(

    <div className="List">
        <ul>
            {props.listItems.map(item => (
                <ListItem key={item.id || item.id}  handleOnClick={props.changeAsset} data={item} workspaceData={props.workspaceData}/>
            ))}
        </ul>
    </div>
);

export default List;

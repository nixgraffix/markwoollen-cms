import React from 'react';
import './Nav.css';

const Nav = (props) =>(
    
    <div className="Nav">
        <div className="switch">
            {props.workspaces.map(workspace => {
                let classString = workspace === props.currentWorkspace ? 'active' : null ;
                return (
                    <button className={classString} key={workspace} onClick={() => props.changeWorkspace(workspace)}>
                        {workspace}
                    </button>    
                );
            })}
        </div>
        <div>
            <button className="addNew" onClick={props.handleAddNewClick}>+ add new</button>
        </div>
       
    </div>
);

export default Nav;

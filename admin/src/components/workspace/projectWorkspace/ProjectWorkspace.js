import React, { Component } from 'react';
import './ProjectWorkspace.css';
import TrailerSelector from './TrailerSelector.js';

class ProjectWorkspace extends Component {
    constructor(props) {
        super(props);
    }

    state = {}

    componentDidMount() {
        fetch(window.location.protocol + "//localhost/markwoollen.com/api.php?action=getList&query=Trailers")
        .then(this.handleResponse)
        .then((result) => {
            this.setState({allTrailers : result });
        })
        .catch(error => {})
    }

    handleResponse (response) {
        return response.json()
            .then(json => {
                if (response.ok) {
                    return json
                } else {
                    return Promise.reject(Object.assign({}, json, {
                    status: response.status,
                    statusText: response.statusText
                }))
            }
        })
    }

    createTrailorSelector = (start,stop, project) => {
        let trailers = [];
        for(let i=start;i<=stop;i++){
            trailers.push(<TrailerSelector key={i} count={i} label={"Trailer " + i} data={project} allTrailers={this.state.allTrailers} handleChange={this.props.handleChange}/>);
        }
        return trailers;
    }

    render(){

        let project;

        if(this.props.workspaceData){
            project = this.props.workspaceData;        

            return(
                <div className="ProjectWorkspace">
                    <form>
                        <h1>{project.projectTitle}</h1>
                        <p><small>Created on:{project.createdOn} Last edited: {project.updatedOn}</small></p>
                        <div>
                            <label>Title</label>
                            <input type="text" onChange={this.props.handleChange} name="projectTitle" value={project.projectTitle}/>
                        </div>
                        <div>
                            <label>Site Page</label>
                            <select onChange={this.props.handleChange} name="page">
                                {this.props.pages.map(page => (
                                    <option key={page} value={page}>{page}</option>
                                ))}
                            </select>
                        </div>
                        <div className="radioSection">
                            <label>Publish</label>
                            <input type="radio" onChange={this.props.handleChange} name="publish" value="1" checked={project.publish === 1 ? "checked" : null}/><label>True</label>
                            <input type="radio" onChange={this.props.handleChange} name="publish" value="0" checked={project.publish === 0 ? "checked" : null}/><label>False</label>
                        </div>
                        
                        <TrailerSelector count={0} label="Main Trailer" data={project} allTrailers={this.state.allTrailers} handleChange={this.props.handleChange}/>

                        <div className="row">
                            <div className="column">
                                {this.createTrailorSelector(1,4, project)}
                            </div>
                            <div className="column">
                                {this.createTrailorSelector(5,8, project)}
                            </div>
                        </div>
                        <div className="save">
                            <button className="saveButton"  onClick={this.props.handleSave}>Save</button>
                            <button className="deleteButton" onClick={this.props.handleDelete}>Delete</button>
                        </div>
                        
                    </form>
                </div>
            );

        }else{
            return(
                <div className="ProjectWorkspace"></div>
            )
        }

    }
}

export default ProjectWorkspace;
import React from 'react';

const TrailerSelector = (props) => {

    let value = "";
    let trailers = []; 
    
    if( props.data.hasOwnProperty("trailers")
        && props.data.trailers[props.count] 
        && props.data.trailers[props.count].hasOwnProperty('assetID')
        && props.data.trailers[props.count].assetID != null){

            value = props.data.trailers[props.count].assetID;
    }

    if(props.allTrailers){
        trailers = props.allTrailers;
    }
    
    /*
    return (
        <div>
            {JSON.stringify(props.allTrailers)}
        </div>
    );
    */

    return(
        <div>
            <label>{props.label}</label>
            <select value={value} onChange={props.handleChange} name="assetID" data-array-key={props.count}>
                <option value="null">--- Null ---</option>
                {trailers.map(item => (
                    <option key={item.id} value={item.id}>{item.title}</option>
                ))}
            </select>
        </div>
    )
};
    
export default TrailerSelector;
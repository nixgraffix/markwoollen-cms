import React, { Component } from 'react';
import TrailerPoster from '../trailerPoster/TrailerPoster.js';
import TrailerVideo from '../trailerVideo/TrailerVideo.js';
import './TrailerWorkspace.css';

class TrailerWorkspace extends Component {

    state = {
        imageHash: Date.now()
    }

    save=(event)=>{
        this.props.handleSave(event);
        // do this to reload the img srcs after saving
        setTimeout(() => {
            this.setState({imageHash : Date.now()});
        }, 100)
    }

    render(){
        if(this.props.workspaceData){
            let trailer = this.props.workspaceData;

            return(
                <div className="TrailerWorkspace">
                    <form>
                        <h1>{trailer.title}</h1>
                        <div>
                            <TrailerVideo trailer={trailer} imageHash={this.state.imageHash} handleChange={this.props.handleChange}/>
                        </div>
                        <div>
                            <label>Title</label>
                            <input type="text" onChange={this.props.handleChange} name="title" value={trailer.title}/>
                        </div>
                        <div>
                            <label>Label</label>
                            <input type="text" onChange={this.props.handleChange} name="label" value={trailer.label}/>
                        </div>
                        <div>
                            <label>File Name</label>
                            <input type="text" onChange={this.props.handleChange} name="fileName" value={trailer.fileName}/>
                        </div>
                        <label>Poster/Thumbnail</label>
                            <TrailerPoster trailer={trailer} imageHash={this.state.imageHash} handleChange={this.props.handleChange}/>
                        <div className="save">
                            <button className="saveButton" onClick={this.save}>Save</button>
                            <button className="deleteButton" onClick={this.props.handleDelete}>Delete</button>
                        </div>
                    </form>
                </div>
            );

        }else{
            return(
                <div className="ProjectWorkspace"></div>
            );
        }
    }
}

export default TrailerWorkspace;
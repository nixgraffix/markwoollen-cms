import React, { Component } from 'react';
import './TrailerPoster.css';

class TrailerPoster extends Component {
  
    render() {
        if(this.props.trailer.posterFile){
            return (
                <div className="thumbnailSection row">
                    <div className="column">
                        <img src={this.props.trailer.posterFile} alt={this.props.trailer.title + " poster image"}/>
                    </div>
                    <div className="column">
                        <div className="FileInput">
                            <label>Upload New</label>
                            <input name="posterFile" type="file" accept=".jpg" onChange={this.props.handleChange}/>
                        </div>
                    </div>
                </div>
            );
        }else if(this.props.trailer.fileName){
            return (
                <div className="thumbnailSection row">
                    <div className="column">
                        <img src={"http://localhost/markwoollen.com/img/poster/" + this.props.trailer.fileName + ".jpg?nocache=" + this.props.imageHash} alt={this.props.trailer.title + " poster image"}/>
                    </div>
                    <div className="column">
                        <div className="FileInput">
                            <label>Upload New</label>
                            <input name="posterFile" type="file" accept=".jpg" onChange={this.props.handleChange}/>
                        </div>
                    </div>
                </div>
            );
        }else{
            return (
                <div className="FileInput">
                    <label>Upload New</label>
                    <input name="posterFile" type="file" accept=".jpg" onChange={this.props.handleChange}/>
                </div>
            );
        }

    }
} 

export default TrailerPoster;
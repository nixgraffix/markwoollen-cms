import React, { Component } from 'react';
import './TrailerVideo.css';

class TrailerVideo extends Component {
  
    render() {
        if(this.props.trailer.videoFile){
            return (
                <div className="TrailerVideo">
                    <video controls key={this.props.trailer.fileName} poster={this.props.trailer.posterFile} >
                        <source src={this.props.trailer.videoFile} type="video/mp4" />
                        Your browser does not support the video tag.
                    </video>
                    <label>Upload New</label>
                    <input name="videoFile" type="file" accept=".mp4" onChange={this.props.handleChange}/>
                </div>   
            );
        }else if(this.props.trailer.fileName){
            return (
                <div className="TrailerVideo">
                    <video controls key={this.props.trailer.fileName} poster={"http://localhost/markwoollen.com/img/poster/" + this.props.trailer.fileName + ".jpg?nocache=" + this.props.imageHash} >
                        <source src={"http://www.markwoollen.com/video/" + this.props.trailer.fileName + ".mp4"} type="video/mp4" />
                        Your browser does not support the video tag.
                    </video>
                    <label>Upload New</label>
                    <input name="videoFile" type="file" accept=".mp4" onChange={this.props.handleChange}/>
                </div>   
            );
        }else{
            return (
                <div className="TrailerVideo">
                    <label>Upload New</label>
                    <input name="videoFile" type="file" accept=".mp4" onChange={this.props.handleChange}/>
                </div>
            )

        }
    }
} 

export default TrailerVideo;
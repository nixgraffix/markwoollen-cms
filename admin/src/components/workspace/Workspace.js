import React, { Component } from 'react';
import ProjectWorkspace from './projectWorkspace/ProjectWorkspace';
import TrailerWorkspace from './trailerWorkspace/TrailerWorkspace';

import './Workspace.css';

class Workspace extends Component {

    render(){
        
        if(this.props.currentWorkspace === "Projects"){
            return(
                <div className="Workspace">
                    <ProjectWorkspace allTrailers={this.props.allTrailers} workspaceData={this.props.workspaceData} listItems={this.props.listItems} pages={this.props.pages} handleChange={this.props.handleChange} handleSave={this.props.handleSave} handleDelete={this.props.handleDelete}/>
                </div>
            );
        }else if(this.props.currentWorkspace === "Trailers"){
            return(
                <div className="Workspace">
                    <TrailerWorkspace workspaceData={this.props.workspaceData} pages={this.props.pages} handleChange={this.props.handleChange} handleSave={this.props.handleSave} handleDelete={this.props.handleDelete}/>
                </div>
            );
        }else{
            return(
                <div className="Workspace"></div>
            );
        }
    }
}

export default Workspace;

<?php

    require_once 'config.php';
    require_once 'Database.php';

    session_start();

    $action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
    $query = filter_input(INPUT_GET, 'query', FILTER_SANITIZE_STRING);
    $loggedIn = isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true ? true : false;

    $result = array();

    switch($action){
        case "addTrailer":
        case "updateTrailer":
            $json = file_get_contents('php://input');
            $temp = json_decode($json, true);

            $data = array(
                'id' => isset($temp['id']) ? $temp['id'] : null,
                'title' => isset($temp['title']) ? $temp['title'] : null,
                'label' => isset($temp['label']) ? $temp['label'] : null,
                'fileName' => isset($temp['fileName']) ? $temp['fileName'] : null,
                'posterFile' => isset($temp['posterFile']) ? $temp['posterFile'] : null,
                'videoFile' => isset($temp['videoFile']) ? $temp['videoFile'] : null
            );

            if($data['posterFile'] !== null){
                list($type, $data['posterFile']) = explode(';', $data['posterFile']);
                list($encoding, $data['posterFile'])      = explode(',', $data['posterFile']);
                $data['posterFile'] = base64_decode($data['posterFile']);
                $result['posterFileUpload'] = file_put_contents('img/poster/'.$data['fileName'].".jpg", $data['posterFile']);
            }

            if($data['videoFile'] !== null){
                list($type, $data['videoFile']) = explode(';', $data['videoFile']);
                list($encoding, $data['videoFile'])      = explode(',', $data['videoFile']);
                $data['videoFile'] = base64_decode($data['videoFile']);
                $result['videoFileUpload'] = file_put_contents('video/'.$data['fileName'].".mp4", $data['videoFile']);
            }
            
            if($action == "updateTrailer"){
                try {
                    $project = Database::run(
                        "UPDATE assets
                        SET title = ?, label = ?, fileName = ?
                        WHERE id = ?", array($data['title'], $data['label'], $data['fileName'], $data['id'])
                    );
                    
                    $result['trailerTable'] =  "Updated asset table OK";
               
                }catch (Exception $e) {
                    $result['trailerTableError'] =  "Can't update asset table: " .$e->getMessage();
                }
    
                $result = json_encode($result);
            
            }else{

                $data['createdOn'] = date('Y-m-d G:i:s');
                
                try {
                    $project = Database::run(
                        "INSERT INTO assets (title, fileName, label, createdOn)
                         VALUES (?, ?, ?, ?)", array($data['title'], $data['fileName'], $data['label'],  $data['createdOn']));
                         $data['id'] = Database::__callStatic("lastInsertId", array()
                    );

                    $result['trailerTable'] =  "Added to trailer table OK. New id: " . $data['id'];
               
                }catch (Exception $e) {
                    $result['trailerTableError'] =  "Can't add to trailer table: " .$e->getMessage();
                }
            }
            

        break;
        case "addProject":
        case "updateProject":

            $json = file_get_contents('php://input');
            $temp = json_decode($json, true);
            $tempTrailers = $temp['trailers'];

            $data = array(
                'id' => isset($temp['projectID']) ? $temp['projectID'] : null,
                'title' => isset($temp['projectTitle']) ? $temp['projectTitle'] : null,
                'page' => isset($temp['page']) ? $temp['page'] : null,
                'publish' => isset($temp['publish']) ? $temp['publish'] : null,
                'trailers' => isset($tempTrailers) ? $tempTrailers : null       
            );

            if($action == "updateProject"){
                $data['updatedOn'] = date('Y-m-d G:i:s');
                try {
                    $project = Database::run(
                        "UPDATE projects
                        SET title = ?, page = ?, publish = ?, updatedOn =?
                        WHERE id = ?", array($data['title'], $data['page'], $data['publish'], $data['updatedOn'], $data['id'])
                    );
                    
                    $result['projectTable'] =  "Updated projects table OK";
               
                }catch (Exception $e) {
                    $result['projectTableError'] =  "Can't update projects table: " .$e->getMessage();
                }

            }else if($action == "addProject"){
                
                $data['createdOn'] = date('Y-m-d G:i:s');

                try {
                    $project = Database::run(
                        "INSERT INTO projects (title, page, publish, createdOn)
                         VALUES (?, ?, ?, ?)", array($data['title'], $data['page'], $data['publish'],  $data['createdOn']));
                         $data['id'] = Database::__callStatic("lastInsertId", array()
                    );

                    $result['projectTable'] =  "Added to projects table OK. New id: " . $data['id'];
               
                }catch (Exception $e) {
                    $result['projectTableError'] =  "Can't add to projects table: " .$e->getMessage();
                }
            }

            if($result){
                
                try {
                    $project = Database::run(
                        "DELETE FROM projectAssets
                        WHERE projectID = ?", array($data['id'])
                    );                    
                    $result['projectAssetsDeleted'] =  "Old project assets relation deleted: OK";

                }catch (Exception $e) {
                    $result['projectAssetsTableError'] =  "Can't delete from projectAssets table: " .$e->getMessage();
                }

                // TODO : this check doesnt work. it always passes.
                if(array_filter($data['trailers'])){

                    $sqlVals = array();
                    $valIds = array();
                    foreach ($data['trailers'] as $trailer) {
                        if($trailer['assetID'] !== null && $trailer['assetID'] !== ""){  
                            array_push($sqlVals, "(?,?)");
                            array_push($valIds, $data['id'], $trailer['assetID']);     
                        }
                    }
                    $sqlVals = implode(",", $sqlVals);
                    $sqlStatement = "INSERT INTO projectAssets (projectID, assetID) VALUES " .$sqlVals;
                    $result['sqlstatement'] = $sqlStatement;
                    $result['sqlVals'] = implode(",", $valIds);
                    try {
                        Database::run($sqlStatement, $valIds);
                        $result['projectAssetsAdded'] =  "Project assets added: OK";
                    }catch (Exception $e) {
                        $result['projectAssetsError'] =  "Can't update projectAssets table: " .$e->getMessage();
                    }
                }
            }
            
            $result = json_encode($result);

        break;
        case "deleteProject":
        case "deleteTrailer":
            
            $json = file_get_contents('php://input');
            $temp = json_decode($json, true);

            if($action == "deleteProject"){
                $table = "projects";
                $data = array(
                    'id' => isset($temp['projectID']) ? $temp['projectID'] : null 
                );
            }else{
                $table = "assets";
                $data = array(
                    'id' => isset($temp['id']) ? $temp['id'] : null 
                );
            }


            $table = $action == "deleteProject" ? "projects" : "assets";

            try {
                $project = Database::run(
                    "UPDATE ".$table."
                    SET deleted = TRUE
                    WHERE id = ?", array($data['id'])
                );
                $result['trailertTable'] =  "Deleted trailer OK";
            
            }catch (Exception $e) {
                $result['projectTableError'] =  "Can't delete from trailers table: " .$e->getMessage();
            }

            $result = json_encode($result);

        break;
        case "getList" :

            $queryMap = array(
                "Projects" => "projects",
                "Trailers" => "assets"
            );

            if(array_key_exists($query, $queryMap)){
                $items = Database::run("SELECT * FROM " .$queryMap[$query]. " WHERE deleted IS NOT TRUE")->fetchAll();
                usort($items, function($a, $b) {
                    return $b['id'] - $a['id'];
                });       
                $result = json_encode($items);
            }
            
        break;
        case "getProject" :

            $numberOfProjectColumns = 7; // this has to match the number of columns from the project table which have to be first

            $project = Database::run(
                "SELECT p.id as projectID, p.title as projectTitle, p.posterImg, p.createdOn, p.updatedOn, p.sortOrder as productSortOrder, p.page, p.publish, a.title as asssetTitle, a.fileName, a.id as assetID, a.label, a.sortOrder as assetSortOrder
                FROM projects as p
                LEFT JOIN projectAssets as pa
                ON (p.id = pa.projectID) 
                LEFT JOIN assets as a
                ON (pa.assetID = a.id)
                WHERE p.id = ?", 
                array($query)
            )->fetchAll();             

            $tempProject = array_slice($project[0], 0, $numberOfProjectColumns);
            $tempTrailers = array();
            foreach($project as $p){
                array_push($tempTrailers, array_splice($p, $numberOfProjectColumns));
            }

            usort($tempTrailers, function($a, $b) {
                return $a['assetSortOrder'] - $b['assetSortOrder'];
            });

            $tempProject["trailers"] = $tempTrailers;
            
            $result = json_encode($tempProject);

        break;
        case "getTrailer" :

            $project = Database::run(
                "SELECT id, title, fileName, label, createdOn, updatedOn, sortOrder
                FROM assets 
                WHERE id = ?", 
                array($query)
            )->fetchAll();             

            $result = json_encode($project);

        break;
        default : 
            $result = json_encode(array("error" => "invalid action"));
    }

    header("Access-Control-Allow-Origin: *");
    echo $result;



    //$all = Database::run("SELECT name,id FROM trailers")->fetchAll(PDO::FETCH_KEY_PAIR);
    //var_export($all);
    

    //$one = Database::run("SELECT name FROM trailers WHERE id = ?", array('songwriter'))->fetchAll();
    //var_dump($one);
